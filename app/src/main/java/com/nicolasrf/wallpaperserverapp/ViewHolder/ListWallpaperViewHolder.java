package com.nicolasrf.wallpaperserverapp.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nicolasrf.wallpaperserverapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperserverapp.R;

public class ListWallpaperViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ItemClickListener itemClickListener;

    public TextView textView;
    public TextView textView2;

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public ListWallpaperViewHolder(View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.text_view);
        textView2 = itemView.findViewById(R.id.text_view2);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view,getAdapterPosition());

    }
}
