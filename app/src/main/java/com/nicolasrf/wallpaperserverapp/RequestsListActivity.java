package com.nicolasrf.wallpaperserverapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.nicolasrf.wallpaperserverapp.Common.Common;
import com.nicolasrf.wallpaperserverapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperserverapp.Model.Reported;
import com.nicolasrf.wallpaperserverapp.Model.Request;
import com.nicolasrf.wallpaperserverapp.ViewHolder.ListWallpaperViewHolder;

public class RequestsListActivity extends AppCompatActivity {

    //Firebase
    FirebaseDatabase firebaseDatabase;
    DatabaseReference requests, backgrounds;
    //Firebase Ui Adapter
    FirebaseRecyclerOptions<Request> options;
    FirebaseRecyclerAdapter<Request,ListWallpaperViewHolder> adapter;
    //View
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests_list);

        firebaseDatabase = FirebaseDatabase.getInstance();
        //tokens = firebaseDatabase.getReference("tokens");

        recyclerView = findViewById(R.id.requests_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(RequestsListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);

        requests = FirebaseDatabase.getInstance().getReference("Requests");
        //backgrounds = FirebaseDatabase.getInstance().getReference("Backgrounds");


        options = new FirebaseRecyclerOptions.Builder<Request>()
                .setQuery(requests,Request.class) //select all
                .build();

        adapter = new FirebaseRecyclerAdapter<Request, ListWallpaperViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ListWallpaperViewHolder holder, int position, @NonNull final Request model) {

                holder.textView.setText(model.getUserId());
                holder.textView2.setText(adapter.getRef(position).getKey());

                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Common.request_selected = model;
                        Common.request_key = adapter.getRef(position).getKey();
                        Intent intent = new Intent(RequestsListActivity.this,ViewRequestActivity.class);
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public ListWallpaperViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wallpaper_item, parent, false);
                return new ListWallpaperViewHolder(itemView);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }
}
