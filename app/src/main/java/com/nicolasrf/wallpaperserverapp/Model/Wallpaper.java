package com.nicolasrf.wallpaperserverapp.Model;

public class Wallpaper {
    String categoryId;
    String imageUrl;

    public Wallpaper() {
    }

    public Wallpaper(String categoryId, String imageUrl) {
        this.categoryId = categoryId;
        this.imageUrl = imageUrl;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
