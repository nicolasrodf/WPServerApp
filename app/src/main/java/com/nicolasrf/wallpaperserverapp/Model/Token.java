package com.nicolasrf.wallpaperserverapp.Model;

public class Token {
    public String user_id, token, isServerToken;

    public Token() {
    }

    public Token(String phone, String token, String isServerToken) {
        this.user_id = phone;
        this.token = token;
        this.isServerToken = isServerToken;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIsServerToken() {
        return isServerToken;
    }

    public void setIsServerToken(String isServerToken) {
        this.isServerToken = isServerToken;
    }
}
