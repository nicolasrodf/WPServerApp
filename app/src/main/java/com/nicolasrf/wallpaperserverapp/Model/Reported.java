package com.nicolasrf.wallpaperserverapp.Model;

public class Reported {
    String wallpaperId;
    String reason;
    String imageUrl;
    String imageFull;
    String categoryId;

    public Reported() {
    }

    public Reported(String wallpaperId, String reason, String imageUrl, String imageFull, String categoryId) {
        this.wallpaperId = wallpaperId;
        this.reason = reason;
        this.imageUrl = imageUrl;
        this.imageFull = imageFull;
        this.categoryId = categoryId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageFull() {
        return imageFull;
    }

    public void setImageFull(String imageFull) {
        this.imageFull = imageFull;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getWallpaperId() {
        return wallpaperId;
    }

    public void setWallpaperId(String wallpaperId) {
        this.wallpaperId = wallpaperId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
