package com.nicolasrf.wallpaperserverapp.Model;

import java.util.Date;

public class Request {
    String categoryId;
    String imageUrl;
    String imageFull;
    String userId;
    String userName;
    String date_created;

    public Request() {
    }

    public Request(String categoryId, String imageUrl, String imageFull, String userId, String userName) {
        this.categoryId = categoryId;
        this.imageUrl = imageUrl;
        this.imageFull = imageFull;
        this.userId = userId;
        this.userName = userName;
    }

    public Request(String categoryId, String imageUrl, String imageFull, String userId, String userName, String date_created) {
        this.categoryId = categoryId;
        this.imageUrl = imageUrl;
        this.imageFull = imageFull;
        this.userId = userId;
        this.userName = userName;
        this.date_created = date_created;
    }

    public String getDate_created() {
        return date_created;
    }

    public void setDate_created(String date_created) {
        this.date_created = date_created;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageFull() {
        return imageFull;
    }

    public void setImageFull(String imageFull) {
        this.imageFull = imageFull;
    }
}
