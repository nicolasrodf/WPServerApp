package com.nicolasrf.wallpaperserverapp.Interface;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}
