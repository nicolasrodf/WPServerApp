package com.nicolasrf.wallpaperserverapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nicolasrf.wallpaperserverapp.Common.Common;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class ViewRequestActivity extends AppCompatActivity {
    private static final String TAG = "ViewRequestActivity";

    String lastKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_request);

        Toast.makeText(this, "wallpaper id  " + Common.request_key, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onCreate: IMAGE URL " + Common.request_selected.getImageFull());

        ImageView wallpaperImageView = findViewById(R.id.wallpaper_image_view);

        Picasso.get().load(Common.request_selected.getImageFull()).into(wallpaperImageView);

        //Get Last Key
        getLastBackgroundKey();

        //ACCEPT
        Button acceptButton = findViewById(R.id.accept_button);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Added to Backgrounds
                int key = Integer.valueOf(lastKey)+1;
                Map<String,Object> userBackground = new HashMap<>();
                userBackground.put("userId", Common.request_selected.getUserId());
                userBackground.put("userName", Common.request_selected.getUserName());
                userBackground.put("categoryId",Common.request_selected.getCategoryId());
                userBackground.put("imageUrl",Common.request_selected.getImageUrl());
                //userBackground.put("imageFull",Common.request_selected.getImageFull());
                userBackground.put("date_created",Common.request_selected.getDate_created());
                userBackground.put("id",key);

                FirebaseDatabase.getInstance().getReference("Backgrounds")
                        .child(String.valueOf(key))
                        .setValue(userBackground)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ViewRequestActivity.this, "Upload image to Backgrounds succesfull", Toast.LENGTH_SHORT).show();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ViewRequestActivity.this, "Error uploading to Backgrounds node: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                //Added to User ABcgkrpoudns
                FirebaseDatabase.getInstance().getReference("users")
                .child(Common.request_selected.getUserId())
                .child("uploads")
                .child(String.valueOf(key)) //key backgrounds size + 1
                .setValue(userBackground) //To Community Category!
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(ViewRequestActivity.this, "Uploaded To Database success!", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(ViewRequestActivity.this, "Error uploading to Backgrounds node: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

                //delete from Requests.
                //database
                FirebaseDatabase.getInstance().getReference("Requests")
                        .child(Common.request_key)
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Toast.makeText(ViewRequestActivity.this, "Image deleted", Toast.LENGTH_SHORT).show();
                                Log.d(TAG, "onSuccess: DELETED FROM REQUESTS");
                                finish(); //finish activity
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: ERROR: " +e.getMessage());
                        Toast.makeText(ViewRequestActivity.this, "Error deleting image :"+e.getMessage(), Toast.LENGTH_SHORT).show();
                        finish(); //finish activity
                    }
                });
            }
        });

        //DENIED FUNCITONALLITY
        Button deniedButton = findViewById(R.id.denied_button);
        deniedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //delte functionallity

                //database
                FirebaseDatabase.getInstance().getReference("Requests")
                        .child(Common.request_key)
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: DELETED FROM REQUESTS");
                                finish(); //finish activity
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: ERROR: " +e.getMessage());
                        finish(); //finish activity
                    }
                });

                //Delete from Storage
                // Create a storage reference from our app
                FirebaseStorage storage = FirebaseStorage.getInstance();
                //StorageReference storageRefFull = storage.getReferenceFromUrl(Common.request_selected.getImageFull());
                StorageReference storageRefUrl = storage.getReferenceFromUrl(Common.request_selected.getImageUrl());

                // Delete the file full
//                storageRefFull.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//                        // File deleted successfully
//                        Log.d(TAG, "onSuccess: DELETED IMAGE FULL FROM STORAGE");
//                        finish(); //finish activity
//                    }
//                }).addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception exception) {
//                        // Uh-oh, an error occurred!
//                        Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
//                        finish(); //finish activity
//                    }
//                });
                // Delete the file url
                storageRefUrl.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // File deleted successfully
                        Log.d(TAG, "onSuccess: DELETED IMAGE URL FROM STORAGE");
                        finish(); //finish activity
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                        Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
                        finish(); //finish activity
                    }
                });


            }
        });
    }

    private void getLastBackgroundKey(){
        Query query = FirebaseDatabase.getInstance().getReference("Backgrounds").orderByKey().limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    lastKey = child.getKey();
                    Toast.makeText(ViewRequestActivity.this, "LAST KEY: " + lastKey, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
