package com.nicolasrf.wallpaperserverapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.nicolasrf.wallpaperserverapp.Common.Common;
import com.nicolasrf.wallpaperserverapp.Model.CategoryItem;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.UUID;

import dmax.dialog.SpotsDialog;
import id.zelory.compressor.Compressor;

public class UploadActivity extends AppCompatActivity {
    private static final String TAG = "UploadWallpaperActivity";

    ImageView previewImageView;
    Button uploadButton,browserButton;
    MaterialSpinner spinner;

    //aMaterial sponner data
    Map<String,String> spinnerData = new HashMap<>();

    private Uri filePath;

    String directUrl="";
    String imageCompressString="";
    String fileName="";

    FirebaseStorage storage;
    StorageReference storageReference;

    AlertDialog dialog;

    private File compressedImageFile;

    String lastKey;

    String categoryIdSelect="";

    TextView textView;

    String date_created;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);

        //firebase storage
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        //create dialog to show after upload to storage and before set image in database
        dialog = new SpotsDialog(UploadActivity.this);

        //View
        previewImageView = findViewById(R.id.image_preview);
        browserButton = findViewById(R.id.browser_button);
        uploadButton = findViewById(R.id.upload_button);
        spinner = findViewById(R.id.spinner);

        textView = findViewById(R.id.text_view);

        //Load Spinner data
        loadCategoryToSpinner();

        //Button event
        browserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spinner.getSelectedIndex() == 0) //Hint, not choose anymore
                {
                    Toast.makeText(UploadActivity.this, "Please choose category!", Toast.LENGTH_SHORT).show();
                } else {
                    uploadImage();
                }

            }
        });

        //DATE*****

        int month = getDateGregorian().getMonth() + 1;
        int year = getDateGregorian().getYear() + 1900;
        String minutes = String.valueOf(getDateGregorian().getMinutes());
        if(getDateGregorian().getMinutes() >=0 && getDateGregorian().getMinutes()<=9)
            minutes = new StringBuilder().append("0").append(getDateGregorian().getMinutes()).toString();

        //currentDateGregorian = getDateGregorian();
        date_created = new StringBuilder()
                .append(getDateGregorian().getDate())
                .append("/")
                .append(month)
                .append("/")
                .append(year)
                .append(" ")
                .append(getDateGregorian().getHours())
                .append(":")
                .append(minutes)
                .toString();

        getLastBackgroundKey();
//Toast.makeText(this, "day " +getDateGregorian().getDay() + "mint "+getDateGregorian().getMonth(), Toast.LENGTH_SHORT).show();

    }

    public static Date getDateGregorian(){
        TimeZone timeZone = TimeZone.getDefault();
        Date dateGregorian = new GregorianCalendar(timeZone).getTime();
        return dateGregorian;
    }

    private void sendToDatabase() {

        //Added to Backgrounds
        int key = Integer.valueOf(lastKey)+1;
        Map<String,Object> data = new HashMap<>();
        data.put("id",key);
        data.put("imageUrl", imageCompressString);
        //data.put("imageFull", directUrl);
        data.put("categoryId", categoryIdSelect);
        data.put("userId", "server001"); //NEW FIELD
        data.put("userName", textView.getText().toString()); //NEW FIELD
        data.put("date_created", date_created); //NEW FIELD
        FirebaseDatabase.getInstance().getReference("Backgrounds")
                .child(String.valueOf(key))
                .setValue(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(UploadActivity.this, "Upload image to Backgrounds succesfull", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(UploadActivity.this, "Error uploading to Backgrounds node: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadCategoryToSpinner() {
        FirebaseDatabase.getInstance()
                .getReference("CategoryBackground")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for(DataSnapshot postSnapshot:dataSnapshot.getChildren()){
                            CategoryItem item = postSnapshot.getValue(CategoryItem.class);
                            String key = postSnapshot.getKey();

                            spinnerData.put(key,item.getName());
                        }

                        //Becasuee Material spinner will not receive hint se we need custom hint
                        //Tghis is my tip
                        Object[] valueArray = spinnerData.values().toArray();
                        List<Object> valueList = new ArrayList<>();
                        valueList.add(0,"Category"); //We will add first item is Hint
                        valueList.addAll(Arrays.asList(valueArray)); //And add all remain cateogry name
                        spinner.setItems(valueList); //set source data for spinner
                        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                                //When user choose category, we will get categoryId (key)
                                Object[] keyArray = spinnerData.keySet().toArray();
                                List<Object> keyList = new ArrayList<>();
                                keyList.add(0,"Category_Key");
                                keyList.addAll(Arrays.asList(keyArray));
                                categoryIdSelect = keyList.get(position).toString(); //Assign key when user choose category
                            }
                        });
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }


    private void deleteFileFromStorage(final String fileName) {
        storageReference.child("images/"+fileName)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(UploadActivity.this, "Your image is adult content and will be deleted", Toast.LENGTH_SHORT).show();
                    }
                });
        storageReference.child("images/"+fileName+"_compressed")
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        finish();
                    }
                });
    }

    public void checkPermissions(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(UploadActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                Toast.makeText(UploadActivity.this, "Permiso denegado", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(UploadActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                initImageCropper();
            }
        } else {
            initImageCropper();
        }
    }

    private void uploadImage() {

        if(filePath != null){

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            //compress before upload

            //create file
            File file = new File(filePath.getPath());

            try {
                compressedImageFile = new Compressor(UploadActivity.this)
                        .setMaxHeight(1920)
                        .setMaxWidth(1080)
                        .setQuality(50)
                        .compressToFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //create Uri from file (compressed in this case)
            final Uri fileCompressedPath = Uri.fromFile(compressedImageFile);

            fileName = UUID.randomUUID().toString();

            //2 tasks: no compress and with compressing

                                //2
                                final StorageReference ref = storageReference.child(new StringBuilder("images/").append(fileName).append("_compressed").toString());
                                final UploadTask uploadTask  = ref.putFile(fileCompressedPath);
                                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                    @Override
                                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                        progressDialog.dismiss();
                                        //GET DOWNLOAD URI
                                        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                            @Override
                                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                                if (!task.isSuccessful()) {
                                                    throw task.getException();
                                                }
                                                return ref.getDownloadUrl();

                                            }
                                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Uri> task) {
                                                if (task.isSuccessful()) {
                                                    Uri imageCompressUri = task.getResult(); //  URI obtenido desde el urlTask! con el metodo del return anterior!
                                                    Log.d(TAG, "onComplete: DOWNLOAD URI " + imageCompressUri);
                                                    imageCompressString = imageCompressUri.toString();
                                                    //Before create Spot dialog
                                                    dialog.show();
                                                    dialog.setMessage("Por favor espere..");
                                                    sendToDatabase();
                                                    //
                                                    //NOT YET, WILL UPLOAD TO DATABASE IN SERVER APP! ONLY WILL BE SEND TO REQUESTS NODE
                                                    //saveUrlToDatabase(categoryIdSelect, directUrl, imageCompressString);
                                                }
                                            }
                                        });

                                    }
                                })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                progressDialog.dismiss();
                                                Toast.makeText(UploadActivity.this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        })
                                        .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                                double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                                                progressDialog.setMessage("Uploaded: " + (int)progress+"%");
                                            }
                                        });

        }
    }

    private void getLastBackgroundKey(){
        Query query = FirebaseDatabase.getInstance().getReference("Backgrounds").orderByKey().limitToLast(1);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    lastKey = child.getKey();
                    Toast.makeText(UploadActivity.this, "LAST KEY: " + lastKey, Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {

                filePath = result.getUri();

                //previewImageView.setImageURI(filePath);
                Picasso.get().load(filePath).fit().into(previewImageView);
                uploadButton.setEnabled(true);
                //uploadImage();

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {

                Exception error = result.getError();
                Toast.makeText(UploadActivity.this, "Error en recortar la imagen.", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void initImageCropper() {
        //Set crop properties.
        //Just accept images cropped with Android device screen aspect ratio
        CropImage.activity()
                .setFixAspectRatio(true)
                .setAspectRatio(9,16)
                .setInitialCropWindowPaddingRatio(0)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setMinCropResultSize(1920, 1080)
                .setActivityTitle("RECORTAR")
                .setCropMenuCropButtonTitle("OK")
                .start(this);
    }


    @Override
    public void onBackPressed() {
        deleteFileFromStorage(fileName);
        super.onBackPressed();
    }
}
