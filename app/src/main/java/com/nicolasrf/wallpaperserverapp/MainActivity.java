package com.nicolasrf.wallpaperserverapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.nicolasrf.wallpaperserverapp.Common.Common;
import com.nicolasrf.wallpaperserverapp.Interface.ItemClickListener;
import com.nicolasrf.wallpaperserverapp.Model.Reported;
import com.nicolasrf.wallpaperserverapp.Model.Token;
import com.nicolasrf.wallpaperserverapp.ViewHolder.ListWallpaperViewHolder;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    //Firebase
    FirebaseDatabase database;
    DatabaseReference reported, backgrounds;
    //Firebase Ui Adapter
    FirebaseRecyclerOptions<Reported> options;
    FirebaseRecyclerAdapter<Reported,ListWallpaperViewHolder> adapter;
    //View
    RecyclerView recyclerView;

    String imageUrl;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference tokens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button requestsButton = findViewById(R.id.delete_button);
        requestsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, RequestsListActivity.class));
            }
        });

        Button uploadButton = findViewById(R.id.upload_button);
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, UploadActivity.class));
            }
        });

        firebaseDatabase = FirebaseDatabase.getInstance();
        tokens = firebaseDatabase.getReference("tokens");

        recyclerView = findViewById(R.id.reported_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);


        reported = FirebaseDatabase.getInstance().getReference("Reported");
        backgrounds = FirebaseDatabase.getInstance().getReference("Backgrounds");


        options = new FirebaseRecyclerOptions.Builder<Reported>()
                .setQuery(reported,Reported.class) //select all
                .build();

        adapter = new FirebaseRecyclerAdapter<Reported, ListWallpaperViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ListWallpaperViewHolder holder, int position, @NonNull final Reported model) {

                holder.textView.setText(model.getWallpaperId());
                holder.textView2.setText(model.getReason());

                holder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Common.reported_selected = model;
                        Intent intent = new Intent(MainActivity.this,ViewBackgroundActivity.class);
                        startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public ListWallpaperViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wallpaper_item, parent, false);
                return new ListWallpaperViewHolder(itemView);
            }
        };

        adapter.startListening();
        recyclerView.setAdapter(adapter);

        updateTokenToServer();

    }

    private void updateTokenToServer() {
        FirebaseInstanceId.getInstance()
                .getInstanceId()
                .addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {

                        //craate or update token collection

                        Token token = new Token("server_app_01",instanceIdResult.getToken(),"1");

                        tokens.child("server_app_01")
                                .setValue(token)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Log.d(TAG, "onSuccess: TOKEN EXITOSO: ");
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d(TAG, "onFailure: ERROR TOKEN:; " + e.getMessage());
                                    }
                                });



                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
