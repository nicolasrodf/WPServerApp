package com.nicolasrf.wallpaperserverapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.nicolasrf.wallpaperserverapp.Common.Common;
import com.squareup.picasso.Picasso;

public class ViewBackgroundActivity extends AppCompatActivity {
    private static final String TAG = "ViewBackgroundActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_background);

        Toast.makeText(this, "wallpaper id  " + Common.reported_selected.getWallpaperId(), Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onCreate: IMAGE URL " + Common.reported_selected.getImageFull());

        ImageView wallpaperImageView = findViewById(R.id.wallpaper_image_view);

        Picasso.get().load(Common.reported_selected.getImageFull()).into(wallpaperImageView);

        Button deleteButton = findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //delte functionallity

                //database
                FirebaseDatabase.getInstance().getReference("Backgrounds")
                        .child(Common.reported_selected.getWallpaperId())
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: DELETED FROM BACKGROUND");
                                finish(); //finish activity
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: ERROR: " +e.getMessage());
                        finish(); //finish activity
                    }
                });
                FirebaseDatabase.getInstance().getReference("Reported")
                        .child(Common.reported_selected.getWallpaperId())
                        .removeValue()
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: DELETED FROM REPORTED");
                                finish(); //finish activity
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: ERROR: " +e.getMessage());
                        finish(); //finish activity
                    }
                });

                //Delete from Storage
                // Create a storage reference from our app
                FirebaseStorage storage = FirebaseStorage.getInstance();
                StorageReference storageRefFull = storage.getReferenceFromUrl(Common.reported_selected.getImageFull());
                StorageReference storageRefUrl = storage.getReferenceFromUrl(Common.reported_selected.getImageUrl());

                // Delete the file full
                storageRefFull.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // File deleted successfully
                        Log.d(TAG, "onSuccess: DELETED IMAGE FULL FROM STORAGE");
                        finish(); //finish activity
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                        Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
                        finish(); //finish activity
                    }
                });
                // Delete the file url
                storageRefUrl.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        // File deleted successfully
                        Log.d(TAG, "onSuccess: DELETED IMAGE URL FROM STORAGE");
                        finish(); //finish activity
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Uh-oh, an error occurred!
                        Log.d(TAG, "onFailure: ERROR: " + exception.getMessage());
                        finish(); //finish activity
                    }
                });


            }
        });



    }
}
